import React, {Component} from 'react';
import {
  View,
  StyleProp,
  ViewStyle,
  TextStyle,
  SafeAreaView,
} from 'react-native';
import {HEADER_HEIGHT} from './const';

interface HeaderProps {
  style?: StyleProp<ViewStyle>;
  backFunction?: () => void;
  backTitle?: string;
  animated?: boolean;
  textStyle?: StyleProp<TextStyle>;
  titleStyle?: StyleProp<ViewStyle>;
  headerRight?: object;
  headerMid?: object;
  headerLeft?: object;
  marginTopHeader?: number;
}

export default class Header extends Component<HeaderProps> {
  render() {
    const {style, headerLeft, headerRight, marginTopHeader} = this.props;
    return (
      <SafeAreaView
        style={[
          {
            backgroundColor: 'transparent',
            zIndex: 100,
            width: '100%',
          },
          style,
        ]}>
        <View
          style={{
            marginTop: marginTopHeader,
            overflow: 'hidden',
            height: HEADER_HEIGHT,
          }}>
          {headerLeft ? (
            <View
              style={{
                position: 'absolute',
                top: 0,
                bottom: 0,
                left: 0,
                justifyContent: 'center',
                alignSelf: 'center',
              }}>
              {headerLeft}
            </View>
          ) : null}
          {headerRight ? (
            <View
              style={{
                position: 'absolute',
                right: 0,
                bottom: 0,
                top: 0,
                justifyContent: 'center',
                alignSelf: 'center',
              }}>
              {headerRight}
            </View>
          ) : null}
        </View>
      </SafeAreaView>
    );
  }
}
