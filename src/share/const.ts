import {Dimensions} from 'react-native';

export const WINDOW_WIDTH = Dimensions.get('window').width;
export const WINDOW_HEIGHT = Dimensions.get('window').height;
export const HEADER_HEIGHT = 75;
export const HEADER_LIST_HEIGHT = WINDOW_WIDTH / 7;

export const fontSize12 = WINDOW_WIDTH / 34;
export const fontSize14 = WINDOW_WIDTH / 30;
export const fontSize16 = WINDOW_WIDTH / 26;
export const fontSize18 = WINDOW_WIDTH / 23;
export const fontSize20 = WINDOW_WIDTH / 21;
export const fontSize24 = WINDOW_WIDTH / 18;
export const fontSize28 = WINDOW_WIDTH / 15;
export const fontSize32 = WINDOW_WIDTH / 13.5;

export const RubikRegular = 'Rubik-Regular';
export const RubikMedium = 'RubikMedium';
export const RubikBold = 'Rubik-Bold';

export const imagesPath = {
  flame: require('../../assets/iconsImages/flame.png'),
  suitcase: require('../../assets/iconsImages/suitcase.png'),
  worldwide: require('../../assets/iconsImages/worldwide.png'),
  politics: require('../../assets/iconsImages/handshake.png'),
};
