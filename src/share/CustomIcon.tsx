import {Image, View} from 'react-native';
import React from 'react';

export const CustomIcon = ({
  imagePath,
  size,
}: {
  imagePath: object;
  size?: number;
}) => {
  return (
    <View
      style={{
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
      }}>
      <Image
        resizeMode={'contain'}
        style={{
          width: size,
          height: size,
        }}
        source={imagePath}
      />
    </View>
  );
};
