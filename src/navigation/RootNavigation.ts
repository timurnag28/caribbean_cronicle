import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import NewsStack from './NewsStack';

const AppNavigator = createSwitchNavigator(
  {
    newsStack: NewsStack,
  },

  {
    initialRouteName: 'newsStack',
  },
);
const AppContainer = createAppContainer(AppNavigator);
export default AppContainer;
