import {createStackNavigator} from 'react-navigation-stack';
import News from '../screens/News';

export default createStackNavigator(
  {
    News: {
      screen: News,
    },
  },
  {
    initialRouteName: 'News',
    headerMode: 'none',
    defaultNavigationOptions: {
      cardStyle: {
        backgroundColor: '#FAFAFA',
      },
    },
  },
);
