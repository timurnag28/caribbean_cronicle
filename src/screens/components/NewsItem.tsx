import {StyleProp, StyleSheet, Text, View, ViewStyle} from 'react-native';
import React from 'react';
import {CustomIcon} from '../../share/CustomIcon';
import {
  fontSize14,
  fontSize16,
  fontSize24,
  imagesPath,
  RubikBold,
  RubikMedium,
  RubikRegular,
} from '../../share/const';

export const NewsItem = ({
  title,
  style,
  hotNews,
  commentsNumber,
  by,
  rowNum,
}: {
  title: string;
  style?: StyleProp<ViewStyle>;
  hotNews: boolean;
  commentsNumber: number;
  by: string;
  rowNum: number;
}) => {
  return (
    <View style={[styles.container, style]}>
      <View style={styles.firstRowContainer}>
        <View style={{flex: 10}}>
          <Text
            numberOfLines={rowNum}
            style={{
              fontFamily: hotNews ? RubikBold : RubikRegular,
              fontSize: 15,
            }}>
            {title}
          </Text>
        </View>
        {hotNews ? (
          <View style={{flex: 1}}>
            <CustomIcon imagePath={imagesPath.flame} size={fontSize24} />
          </View>
        ) : (
          <View style={{flex: 1}} />
        )}
      </View>
      <View style={styles.secondRowContainer}>
        <Text style={styles.comments}>
          <Text style={styles.footerTitles}>{commentsNumber} </Text>
          comments
        </Text>
        <Text style={styles.createdBy}>
          5 days ago by <Text style={styles.footerTitles}>{by}</Text>
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 16,
    paddingHorizontal: 16,
    marginHorizontal: 16,
    marginVertical: 6,
    flexDirection: 'column',
    elevation: 1,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
  },
  firstRowContainer: {flexDirection: 'row', justifyContent: 'space-between'},
  secondRowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 16,
  },

  comments: {
    color: 'rgba(121, 121, 121, 0.5)',
    fontFamily: RubikRegular,
    fontSize: fontSize14,
  },
  createdBy: {
    color: 'rgba(121, 121, 121, 0.5)',
    fontFamily: RubikRegular,
    fontSize: fontSize14,
  },
  footerTitles: {fontFamily: RubikMedium, fontSize: fontSize14},
});
