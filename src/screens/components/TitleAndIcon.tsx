import {StyleProp, StyleSheet, Text, View, ViewStyle} from 'react-native';
import React from 'react';
import {CustomIcon} from '../../share/CustomIcon';
import {
  fontSize16,
  fontSize20,
  RubikMedium,
  RubikRegular,
} from '../../share/const';

export const TitleAndIcon = ({
  title,
  imagePath,
  style,
  size,
}: {
  title: string;
  imagePath: object;
  style?: StyleProp<ViewStyle>;
  size?: number;
}) => {
  return (
    <View style={[styles.container, style]}>
      <Text style={styles.title}>{title}</Text>
      <CustomIcon imagePath={imagePath} size={size} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 16,
    paddingVertical: 6,
    paddingRight: 11,
    paddingLeft: 14,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    borderRadius: 10,
    elevation: 1,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
  },
  title: {
    paddingRight: 16,
    fontFamily: RubikRegular,
    fontSize: fontSize16,
    color: '#707070',
  },
  headerTitle: {
    fontSize: fontSize20,
    color: '#BABABA',
    fontFamily: RubikMedium,
  },

  newsContainer: {width: '100%', flex: 1},
});
