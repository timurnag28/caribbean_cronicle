import React, {Component} from 'react';
import {FlatList, Text, TouchableOpacity, View, StyleSheet} from 'react-native';
import {NavigationProps} from '../share/interfaces';
import Header from '../share/Header';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  fontSize16,
  fontSize20,
  fontSize24,
  fontSize32,
  HEADER_LIST_HEIGHT,
  RubikMedium,
  RubikRegular,
} from '../share/const';
import {TitleAndIcon} from './components/TitleAndIcon';
import {newsData, newsData1} from '../share/info';
import {NewsItem} from './components/NewsItem';

export default class News extends Component<NavigationProps> {
  render() {
    return (
      <View style={styles.container}>
        <Header
          marginTopHeader={41}
          style={styles.header}
          headerLeft={
            <View>
              <Text style={styles.headerTitle}>Caribbean Chronicle</Text>
            </View>
          }
          headerRight={
            <TouchableOpacity onPress={() => alert('onPress')}>
              <Icon
                name={'menu'}
                size={fontSize32}
                color={'rgba(191,179,179,1)'}
              />
            </TouchableOpacity>
          }
        />
        <View style={{height: HEADER_LIST_HEIGHT}}>
          <FlatList
            keyExtractor={item => item.id.toString()}
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            data={newsData}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() => alert('onPressHeaderNews')}
                key={index}
                style={{paddingHorizontal: 12}}>
                <TitleAndIcon
                  title={item.title}
                  imagePath={item.icon}
                  size={fontSize16}
                />
              </TouchableOpacity>
            )}
          />
        </View>
        <View style={styles.newsContainer}>
          <FlatList
            keyExtractor={item => item.id.toString()}
            showsVerticalScrollIndicator={false}
            data={newsData1}
            renderItem={({item, index}) => (
              <TouchableOpacity onPress={() => alert('onPressNews')}>
                <NewsItem
                  key={index}
                  title={item.newsDescription}
                  hotNews={item.hot}
                  commentsNumber={item.commentsNum}
                  by={item.by}
                  rowNum={item.rowNum}
                />
              </TouchableOpacity>
            )}
            ListFooterComponent={
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginVertical: 30,
                }}>
                <Text
                  style={{
                    fontFamily: RubikRegular,
                    color: 'rgba(0, 0, 0, 0.4)',
                  }}>
                  Loading news…
                </Text>
              </View>
            }
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  header: {
    paddingHorizontal: 30,
    borderBottomWidth: 1,
    borderBottomColor: '#00000029',
  },
  headerTitle: {
    fontSize: fontSize20,
    color: '#BABABA',
    fontFamily: RubikMedium,
  },

  newsContainer: {width: '100%', flex: 1},
});
