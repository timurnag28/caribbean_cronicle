import React, {Component} from 'react';
import RootNavigation from './src/navigation/RootNavigation';
import {SafeAreaView, StatusBar, Text, View} from 'react-native';

export default class App extends Component {
  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <StatusBar
          translucent
          backgroundColor="transparent"
          barStyle="dark-content"
        />

          <RootNavigation />
      </SafeAreaView>
    );
  }
}
